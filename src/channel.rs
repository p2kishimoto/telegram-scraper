// Copyright (c) 2022 Yuki Kishimoto
// Distributed under the MIT software license

//use std::collections::HashMap;

use select::document::Document;
use select::predicate::Class;
use url::Url;

#[derive(Debug)]
pub struct Body {
    pub plain_text: String,
    pub html: String,
}

#[derive(Debug)]
pub struct Post {
    pub id: u32,
    pub channel: String,
    //pub photos: Vec<String>,
    pub body: Body,
}

#[derive(Clone)]
pub struct Channel {
    endpoint: String,
    client: reqwest::blocking::Client,
}

#[derive(Debug)]
pub enum ChannelError {
    Reqwest(reqwest::Error),
    BadResult,
    Unauthorized,
    BadRequest,
    Forbidden,
    NotFound,
    MethodNotAllowed,
    TooManyRequests,
    UnhandledClientError,
    InternalServerError,
    NotImplemented,
    BadGateway,
    ServiceUnavailable,
    GatewayTimeout,
    UnhandledServerError,
}

impl Channel {
    pub fn new(name: &str, proxy: Option<&str>) -> Result<Self, ChannelError> {
        let endpoint: String = format!("https://t.me/s/{}", name.to_lowercase());

        let mut client = reqwest::blocking::Client::builder();

        if let Some(proxy) = proxy {
            client = client.proxy(reqwest::Proxy::all(proxy)?);
        }

        Ok(Self {
            endpoint,
            client: client.build()?,
        })
    }

    fn request(&self) -> Result<String, ChannelError> {
        let req = self.client.get(&self.endpoint);
        let res = req.send()?;

        match reqwest::StatusCode::as_u16(&res.status()) {
            0_u16..=399_u16 => Ok(res.text()?),
            400 => Err(ChannelError::BadRequest),
            401 => Err(ChannelError::Unauthorized),
            402 => Err(ChannelError::UnhandledClientError),
            403 => Err(ChannelError::Forbidden),
            404 => Err(ChannelError::NotFound),
            405 => Err(ChannelError::MethodNotAllowed),
            406_u16..=428_u16 => Err(ChannelError::UnhandledClientError),
            429 => Err(ChannelError::TooManyRequests),
            430_u16..=499_u16 => Err(ChannelError::UnhandledClientError),
            500 => Err(ChannelError::InternalServerError),
            501 => Err(ChannelError::NotImplemented),
            502 => Err(ChannelError::BadGateway),
            503 => Err(ChannelError::ServiceUnavailable),
            504 => Err(ChannelError::GatewayTimeout),
            _ => Err(ChannelError::UnhandledServerError),
        }
    }

    pub fn last_posts(&self) -> Result<Vec<Post>, ChannelError> {
        let html: String = self.request()?;
        let document = Document::from(html.as_str());

        let mut posts: Vec<Post> = Vec::new();

        //let mut photos: HashMap<u32, Vec<String>> = HashMap::new();
        let message_texts: Vec<select::node::Node> =
            document.find(Class("tgme_widget_message_text")).collect();

        /* for node in document.find(Class("tgme_widget_message_photo_wrap")) {
            if let Some(href) = node.attr("href") {
                if let Ok(url) = Url::parse(href) {
                    let url_splitted: Vec<&str> = url.path()[1..].split('/').collect();
                    if url_splitted.len() == 2 {
                        if let Ok(id) = url_splitted[1].parse::<u32>() {
                            if let Some(style) = node.attr("style") {
                                let style_splitted: Vec<&str> = style.split(';').collect();

                                for value in style_splitted {
                                    if value.starts_with("background-image") {
                                        let value_len: usize = value.len();

                                        let url: &str = &value[22..value_len - 2];

                                        if let Some(old_photos) = photos.get(&id) {
                                            let mut new_photos = old_photos.clone();
                                            new_photos.push(url.into());
                                            photos.insert(id, new_photos.to_vec());
                                        } else {
                                            photos.insert(id, vec![url.into()]);
                                        }

                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } */

        for (counter, node) in document.find(Class("tgme_widget_message_date")).enumerate() {
            if counter < message_texts.len() {
                if let Some(href) = node.attr("href") {
                    if let Ok(url) = Url::parse(href) {
                        let url_splitted: Vec<&str> = url.path()[1..].split('/').collect();
                        if url_splitted.len() == 2 {
                            if let Ok(id) = url_splitted[1].parse::<u32>() {
                                /* let photo_urls: Vec<String> = match photos.get(&id) {
                                    Some(photos) => photos.to_vec(),
                                    None => vec![],
                                }; */

                                let new_post = Post {
                                    id,
                                    channel: url_splitted[0].into(),
                                    //photos: photo_urls,
                                    body: Body {
                                        plain_text: message_texts[counter].text(),
                                        html: message_texts[counter].inner_html(),
                                    },
                                };

                                posts.push(new_post);
                            }
                        }
                    }
                }
            }
        }

        Ok(posts)
    }
}

impl From<reqwest::Error> for ChannelError {
    fn from(err: reqwest::Error) -> Self {
        ChannelError::Reqwest(err)
    }
}
