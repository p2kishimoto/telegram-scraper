// Copyright (c) 2022 Yuki Kishimoto
// Distributed under the MIT software license

pub mod channel;

pub use channel::Channel;
