// Copyright (c) 2022 Yuki Kishimoto
// Distributed under the MIT software license

extern crate telegram_scraper;

use telegram_scraper::Channel;

fn main() {
    let channel = Channel::new("channel_name", Some("socks5://127.0.0.1:9050")).unwrap();
    let posts = channel.last_posts().unwrap();

    println!("{:#?}", posts);
}
